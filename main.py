import matplotlib.pyplot as plt

x = list(range(0, 200))
y = list(map(lambda x: x**2, x))
plt.plot(x, y)
plt.show()

